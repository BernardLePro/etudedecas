package mines.ales.edc.model;


import org.codehaus.jackson.map.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Initializer {
    public static void init() {
        ObjectMapper mapper = new ObjectMapper();
        List<Image> images = new ArrayList<Image>();
        Image image = new Image();
        image.setNom("IMG_0834.tif");
        image.addObjet(new Objet(7, "Parking payant", "petit"));
        images.add(image);

        image = new Image();
        image.setNom("IMG_0836.tif");
        image.addObjet(new Objet(7, "Novotel Atria", "petit"));
        image.addObjet(new Objet(7, "Hotel Majestic", "petit"));
        image.addObjet(new Objet(7, "Hotel des Tuileries", "petit"));
        image.addObjet(new Objet(7, "Toutes directions", "petit"));
        image.addObjet(new Objet(7, "Uzes", "petit"));
        image.addObjet(new Objet(7, "Acces hotel", "petit"));
        images.add(image);

        image = new Image();
        image.setNom("IMG_0837.tif");
        image.addObjet(new Objet(7, "Novotel Atria", "petit"));
        image.addObjet(new Objet(7, "Hotel Majestic", "petit"));
        image.addObjet(new Objet(7, "Hotel des Tuileries", "petit"));
        image.addObjet(new Objet(7, "Toutes directions", "petit"));
        image.addObjet(new Objet(7, "Uzes", "petit"));
        image.addObjet(new Objet(7, "Acces hotel", "petit"));
        images.add(image);

        image = new Image();
        image.setNom("IMG_0839.tif");
        image.addObjet(new Objet(7, "Porte Auguste", "petit"));
        image.addObjet(new Objet(7, "Coupole Halles", "petit"));
        image.addObjet(new Objet(7, "Office de tourisme", "petit"));
        images.add(image);

        image = new Image();
        image.setNom("IMG_0840.tif");
        image.addObjet(new Objet(7, "Porte Auguste", "petit"));
        image.addObjet(new Objet(7, "Coupole Halles", "petit"));
        image.addObjet(new Objet(7, "Office de tourisme", "petit"));
        image.addObjet(new Objet(7, "Institut de beauté Vera", "grand"));
        images.add(image);

        image = new Image();
        image.setNom("IMG_0841.tif");
        image.addObjet(new Objet(7, "Arènes", "petit"));
        image.addObjet(new Objet(7, "Tuileries", "petit"));
        image.addObjet(new Objet(7, "Kyriad", "petit"));
        image.addObjet(new Objet(7, "Historique", "petit"));
        image.addObjet(new Objet(7, "Pharmacie Notre Dame", "grand"));
        images.add(image);

        image = new Image();
        image.setNom("IMG_0842.tif");
        image.addObjet(new Objet(7, "Impasse Randon", "petit"));
        image.addObjet(new Objet(7, "Panneau stop", "petit"));
        images.add(image);

        image = new Image();
        image.setNom("IMG_0842.tif");
        image.addObjet(new Objet(7, "Impasse Randon", "petit"));
        image.addObjet(new Objet(7, "Panneau stop", "petit"));
        images.add(image);


        try {
            mapper.defaultPrettyPrintingWriter().writeValue(new File("image.txt"), images);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
