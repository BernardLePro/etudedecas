package mines.ales.edc.model;

import ij.ImagePlus;
import mines.ales.edc.controller.model.Utils;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Image {
    public static final String DOSSIER = "/Image/";
    private ImagePlus imagePlus;
    private String nom;
    private List<Objet> objets;

    public Image(String nom) {
        this.nom = nom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Image image = (Image) o;

        if (imagePlus != null ? !imagePlus.equals(image.imagePlus) : image.imagePlus != null) return false;
        if (nom != null ? !nom.equals(image.nom) : image.nom != null) return false;
        if (objets != null ? !objets.equals(image.objets) : image.objets != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = imagePlus != null ? imagePlus.hashCode() : 0;
        result = 31 * result + (nom != null ? nom.hashCode() : 0);
        result = 31 * result + (objets != null ? objets.hashCode() : 0);
        return result;
    }
    public Image(ImagePlus imagePlus, String nom, List<Objet> objets) {
        this.imagePlus = imagePlus;
        this.nom = nom;
        this.objets = objets;
    }

    public Image() {
    }

    public void addObjet(Objet objet) {
        if (objets == null)
            objets = new ArrayList<Objet>();
        objets.add(objet);
    }

    @JsonIgnore
    public BufferedImage getBufferedImage() {
        if (imagePlus == null)
            return null;
        return imagePlus.getBufferedImage();
    }

    @JsonIgnore
    public ImagePlus getImagePlus() {
        imagePlus = new ImagePlus(new Utils().getExecutionPath() + DOSSIER + nom);
        return imagePlus;
    }

    @JsonIgnore
    public void setImagePlus(ImagePlus imagePlus) {
        this.imagePlus = imagePlus;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Objet[] getObjets() {
        if(objets == null) {
            return null;
        } else {
            Collections.sort(objets);
            return objets.toArray(new Objet[objets.size()]);
        }
    }

    public void setObjets(List<Objet> objets) {
        this.objets = objets;
    }
}
