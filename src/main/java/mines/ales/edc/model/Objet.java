package mines.ales.edc.model;

import org.codehaus.jackson.annotate.JsonIgnore;

public class Objet implements Comparable {
    private int x;
    private String contenu;
    private String taille;
    private boolean detecter;

    public Objet(int x, String contenu, String taille) {
        this.x = x;
        this.contenu = contenu;
        this.taille = taille;
    }

    public Objet() {
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getTaille() {
        return taille;
    }

    public void setTaille(String taille) {
        this.taille = taille;
    }

    @JsonIgnore
    public boolean isDetecter() {
        return detecter;
    }

    @JsonIgnore
    public void setDetecter(boolean detecter) {
        this.detecter = detecter;
    }

    public void swapDetecter() {
        this.detecter = !detecter;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("");
        sb.append(detecter ? "X   " : "");
        sb.append(contenu);
        sb.append("  (").append(taille).append(")");
        return sb.toString();
    }

    public String toCSV() {
        final StringBuffer sb = new StringBuffer("");
        sb.append(contenu);
        sb.append(";").append(taille);
        sb.append(";").append(detecter ? "X" : "").append(";");
        return sb.toString();
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof Objet)
            return getContenu().compareTo(((Objet) o).getContenu());
        return 0;
    }
}
