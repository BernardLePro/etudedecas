package mines.ales.edc.controller;

import ij.ImagePlus;
import mines.ales.edc.controller.model.ImageModifier;

public class ImageModifierImpl extends ImageModifier {
    private static int RED = 4;
    private static int GREEN = 2;
    private static int BLUE = 1;

    @Override
    public void changeLuminosity(ImagePlus image, int value) {
        image.setDisplayRange(value, 255);
    }

    @Override
    public void changeRGB(ImagePlus image, int Rvalue, int Gvalue, int Bvalue) {
        image.setDisplayRange(Rvalue, 255, RED);
        image.setDisplayRange(Gvalue, 255, GREEN);
        image.setDisplayRange(Bvalue, 255, BLUE);
    }

}
