package mines.ales.edc.controller;

import mines.ales.edc.controller.model.ImageModifier;

public class Factory {
    static ImageModifier imageModifier;

    public static ImageModifier getImageModifier() {
        if (imageModifier == null)
            imageModifier = new ImageModifierImpl();
        return imageModifier;
    }
}
