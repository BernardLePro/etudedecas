package mines.ales.edc.controller.model;

import mines.ales.edc.callback.Notifier;
import mines.ales.edc.model.Image;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ImageGetter extends Notifier {
    public static final String IMAGE_TXT = "/Image/image.txt";
    static final String[] EXTENSIONS = new String[]{
            "gif", "png", "bmp", "jpg", "JPG", "tif"
    };
    static final FilenameFilter IMAGE_FILTER = new FilenameFilter() {
        @Override
        public boolean accept(final File dir, final String name) {
            for (final String ext : EXTENSIONS) {
                if (name.endsWith("." + ext)) {
                    return (true);
                }
            }
            return (false);
        }
    };
    List<Image> images;
    int position = 0;

    public static ImageGetter getInstance() {
        return SingletonHolder.instance;
    }

    public void getImagesFromFolder() {
        String folder = new Utils().getExecutionPath() + IMAGE_TXT;
        ObjectMapper mapper = new ObjectMapper();
        try {
            images = mapper.readValue(new File(folder), new TypeReference<List<Image>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void getImagesFromFolder(final File folder) {
        if (images == null)
            images = new ArrayList<Image>();
        if (folder.isDirectory()) {
            for (final File fileEntry : folder.listFiles(IMAGE_FILTER)) {

                if (fileEntry.isDirectory()) {
                    getImagesFromFolder(fileEntry);
                } else {
                    images.add(new Image(fileEntry.getName()));
                }
            }
        }
    }

    public Image getCurrentImage() {
        return images.get(position % images.size());
    }

    public boolean hasNextImage() {
        return position < images.size();
    }

    public void goToNextImage() {
        position++;
    }

    private static class SingletonHolder {
        private final static ImageGetter instance = new ImageGetter();
    }
}


