package mines.ales.edc.controller.model;

public class Utils {
    public String getExecutionPath() {
        String absolutePath = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
        absolutePath = absolutePath.substring(0, absolutePath.lastIndexOf("/"));
        absolutePath = absolutePath.replaceAll("%20", " ");
        if (absolutePath.startsWith("/C:/") || absolutePath.startsWith("/D:/"))
            absolutePath = absolutePath.substring(1, absolutePath.length());
        return absolutePath;
    }
}
