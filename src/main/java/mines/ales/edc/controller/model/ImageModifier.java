package mines.ales.edc.controller.model;

import ij.ImagePlus;

public abstract class ImageModifier {

    public abstract void changeLuminosity(ImagePlus image, int value);

    public abstract void changeRGB(ImagePlus image, int Rvalue, int Gvalue, int Bvalue);
}
