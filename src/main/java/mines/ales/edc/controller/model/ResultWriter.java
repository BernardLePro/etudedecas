package mines.ales.edc.controller.model;

import mines.ales.edc.model.Image;
import mines.ales.edc.model.Objet;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ResultWriter {
    FileWriter fileWriter;

    public ResultWriter() {
    }

    public static ResultWriter getInstance() {
        return SingletonHolder.instance;
    }

    public void init(String nom, String date) {
        getFileWriter(getFile(nom, date));
    }

    public File getFile(String nom, String date) {
        String url = new Utils().getExecutionPath() + "/Resultat/" + nom + "/" + date + ".csv";
        Path path = Paths.get(url);
        try {
            Files.createDirectories(path.getParent());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new File(url);
    }

    public void getFileWriter(File file) {
        if (fileWriter == null)
            try {
                fileWriter = new FileWriter(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    public void write(Image image, long timer) {
        Objet[] objets = image.getObjets();
        if (objets.length != 0)
            try {
                fileWriter.append(image.getNom() + ";");
                for (Objet objet : objets) {
                    fileWriter.append(objet.toCSV());
                }
                fileWriter.append(timer + System.getProperty("line.separator"));
                fileWriter.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    public void stop() {
        try {
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static class SingletonHolder {
        private final static ResultWriter instance = new ResultWriter();
    }

}
