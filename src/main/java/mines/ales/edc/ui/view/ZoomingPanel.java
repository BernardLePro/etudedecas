package mines.ales.edc.ui.view;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;

import java.awt.*;
import java.awt.image.BufferedImage;

import static java.awt.RenderingHints.*;

public class ZoomingPanel extends WebcamPanel {
    GraphicsEnvironment ge;
    GraphicsDevice graphicsDevice;
    int maxHeight;
    int maxWidth;
    int minWidth = 0;
    int minHeight = 0;
    float zoom = 0;


    public ZoomingPanel(Webcam webcam) {
        super(webcam);
        setPainter(new ZoomingPainter());
    }

    public void setZoom(float zoom) {
        this.zoom = zoom;
    }

    public class ZoomingPainter implements Painter {
        private long lastRepaintTime = -1;

        private BufferedImage resizedImage = null;

        @Override
        public void paintPanel(WebcamPanel owner, Graphics2D g2) {

            assert owner != null;
            assert g2 != null;

            Object antialiasing = g2.getRenderingHint(KEY_ANTIALIASING);

            g2.setRenderingHint(KEY_ANTIALIASING, isAntialiasingEnabled() ? VALUE_ANTIALIAS_ON : VALUE_ANTIALIAS_OFF);
            g2.setBackground(Color.BLACK);
            g2.fillRect(0, 0, getWidth(), getHeight());

            int cx = (getWidth() - 70) / 2;
            int cy = (getHeight() - 40) / 2;

            g2.setStroke(new BasicStroke(2));
            g2.setColor(Color.LIGHT_GRAY);
            g2.fillRoundRect(cx, cy, 70, 40, 10, 10);
            g2.setColor(Color.WHITE);
            g2.fillOval(cx + 5, cy + 5, 30, 30);
            g2.setColor(Color.LIGHT_GRAY);
            g2.fillOval(cx + 10, cy + 10, 20, 20);
            g2.setColor(Color.WHITE);
            g2.fillOval(cx + 12, cy + 12, 16, 16);
            g2.fillRoundRect(cx + 50, cy + 5, 15, 10, 5, 5);
            g2.fillRect(cx + 63, cy + 25, 7, 2);
            g2.fillRect(cx + 63, cy + 28, 7, 2);
            g2.fillRect(cx + 63, cy + 31, 7, 2);

            g2.setColor(Color.DARK_GRAY);
            g2.setStroke(new BasicStroke(3));
            g2.drawLine(0, 0, getWidth(), getHeight());
            g2.drawLine(0, getHeight(), getWidth(), 0);

            g2.setRenderingHint(KEY_ANTIALIASING, antialiasing);
        }

        @Override
        public void paintImage(WebcamPanel owner, BufferedImage image, Graphics2D g2) {

            assert owner != null;
            assert image != null;
            assert g2 != null;

            int pw = getWidth();
            int ph = getHeight();
            int iw = image.getWidth();
            int ih = image.getHeight();

            Object antialiasing = g2.getRenderingHint(KEY_ANTIALIASING);
            Object rendering = g2.getRenderingHint(KEY_RENDERING);

            g2.setRenderingHint(KEY_ANTIALIASING, VALUE_ANTIALIAS_OFF);
            g2.setRenderingHint(KEY_RENDERING, VALUE_RENDER_SPEED);
            g2.setBackground(Color.BLACK);
            g2.setColor(Color.BLACK);
            g2.fillRect(0, 0, pw, ph);

            // resized image position and size
            int x = 0;
            int y = 0;


            if (resizedImage != null) {
                resizedImage.flush();
            }

            if (pw == image.getWidth() && ph == image.getHeight()) {
                resizedImage = image;
            } else {
                GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
                GraphicsConfiguration gc = genv.getDefaultScreenDevice().getDefaultConfiguration();
                Graphics2D gr = null;
                try {
                    resizedImage = gc.createCompatibleImage(pw, ph);
                    gr = resizedImage.createGraphics();
                    gr.setComposite(AlphaComposite.Src);
                    gr.setBackground(Color.BLACK);
                    gr.setColor(Color.BLACK);
                    gr.fillRect(0, 0, pw, ph);

                    int sx1, sx2, sy1, sy2; // source rectangle coordinates
                    int dx1, dx2, dy1, dy2; // destination rectangle coordinates

                    dx1 = x;
                    dy1 = y;
                    dx2 = x + pw;
                    dy2 = y + ph;

                    sx1 = 0;
                    sy1 = 0;
                    sx2 = iw;
                    sy2 = ih;

                    gr.drawImage(image, dx1, dy1, dx2, dy2, sx1, sy1, sx2, sy2, null);

                } finally {
                    if (gr != null) {
                        gr.dispose();
                    }
                }
            }
            ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            if (ge.getScreenDevices().length > 1) {
                graphicsDevice = ge.getScreenDevices()[1];
            } else {
                graphicsDevice = ge.getDefaultScreenDevice();
            }
            minWidth = 0;
            minHeight = 0;
            maxHeight = graphicsDevice.getFullScreenWindow().getHeight();
            maxWidth = graphicsDevice.getFullScreenWindow().getWidth();

            minWidth -= (int) ((maxWidth - minWidth) / 2 * zoom / 100);
            maxWidth += (int) ((maxWidth - minWidth) / 2 * zoom / 100);
            minHeight -= (int) ((maxHeight - minHeight) / 2 * zoom / 100);
            maxHeight += (int) ((maxHeight - minHeight) / 2 * zoom / 100);
            g2.drawImage(resizedImage, minWidth, minHeight, maxWidth, maxHeight, null);

            g2.setRenderingHint(KEY_ANTIALIASING, antialiasing);
            g2.setRenderingHint(KEY_RENDERING, rendering);
        }
    }
}
