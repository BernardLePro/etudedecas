package mines.ales.edc.ui.view;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public class ImageComponent extends JPanel {
    BufferedImage img;
    GraphicsEnvironment ge;
    GraphicsDevice graphicsDevice;
    int maxHeight;
    int maxWidth;
    int minWidth = 0;
    int minHeight = 0;


    @Override
    public void paintComponent(Graphics g) {
        if (img != null)
            g.drawImage(img, minWidth, minHeight, maxWidth, maxHeight, null);
    }


    public void changeImage(BufferedImage image, float zoom) {
        img = image;
        ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        if (ge.getScreenDevices().length > 1) {
            graphicsDevice = ge.getScreenDevices()[1];
        } else {
            graphicsDevice = ge.getDefaultScreenDevice();
        }
        minWidth = 0;
        minHeight = 0;
        maxHeight = graphicsDevice.getFullScreenWindow().getHeight();
        maxWidth = graphicsDevice.getFullScreenWindow().getWidth();

        minWidth -= (int) ((maxWidth - minWidth) / 2 * zoom / 100);
        maxWidth += (int) ((maxWidth - minWidth) / 2 * zoom / 100);
        minHeight -= (int) ((maxHeight - minHeight) / 2 * zoom / 100);
        maxHeight += (int) ((maxHeight - minHeight) / 2 * zoom / 100);
        repaint();
    }
}