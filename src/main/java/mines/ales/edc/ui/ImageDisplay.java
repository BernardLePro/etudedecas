package mines.ales.edc.ui;

import mines.ales.edc.callback.ImageListener;
import mines.ales.edc.ui.view.ImageComponent;

import java.awt.*;
import java.awt.image.BufferedImage;

public class ImageDisplay implements ImageListener {
    protected Frame frame;
    private ImageComponent imageComponent;

    public ImageDisplay() {
    }

    public void getInstance() {
        frame = new Frame();
        frame.setUndecorated(true);
        imageComponent = new ImageComponent();
        GraphicsEnvironment ge;
        frame.add(imageComponent);
        ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        if (ge.getScreenDevices().length > getPositionEcran()) {
            ge.getScreenDevices()[getPositionEcran()].setFullScreenWindow(frame);
        } else {
            ge.getDefaultScreenDevice().setFullScreenWindow(frame);
        }
        frame.validate();
    }

    public int getPositionEcran() {
        return 1;
    }

    @Override
    public void onImageChanged(BufferedImage image, float zoom) {
        imageComponent.changeImage(image, zoom);
    }
}