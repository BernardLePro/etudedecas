package mines.ales.edc.ui;

import mines.ales.edc.callback.ImageListener;
import mines.ales.edc.callback.Notifier;
import mines.ales.edc.callback.UserEventListener;
import mines.ales.edc.controller.model.ImageGetter;
import mines.ales.edc.controller.model.Utils;
import mines.ales.edc.model.Image;
import mines.ales.edc.model.Objet;
import org.codehaus.jackson.map.ObjectMapper;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;


public class EvaluationUI implements ActionListener, UserEventListener, ImageListener {
    static JFrame frame = new JFrame("EvaluationUI");
    private JSpinner spinnerPanneaux;
    private JSlider sliderComplex;
    private JTextPane textPanePanneaux;
    private JPanel rootPanel;
    private JButton buttonPrec;
    private JButton buttonNext;
    private JButton buttonOK;
    private ImageGetter imageGetter = ImageGetter.getInstance();
    private File file = new File(new Utils().getExecutionPath() + "/Image/image.txt");
    private ObjectMapper mapper = new ObjectMapper();
    private ArrayList<Image> imageList = new ArrayList<Image>();

    public EvaluationUI() {
        imageGetter.getImagesFromFolder(new File(new Utils().getExecutionPath() + "/Image/"));
        Hashtable labelTable = new Hashtable();
        labelTable.put(new Integer(0), new JLabel("0"));
        labelTable.put(new Integer(5), new JLabel("5"));
        labelTable.put(new Integer(10), new JLabel("10"));
        sliderComplex.setLabelTable(labelTable);

        SpinnerModel spinnerModel = new SpinnerNumberModel(0, 0, 1000, 1);
        spinnerPanneaux.setModel(spinnerModel);
        sliderComplex.setMajorTickSpacing(5);
        sliderComplex.setMinorTickSpacing(1);
        sliderComplex.setPaintTicks(true);
        sliderComplex.setPaintLabels(true);
        sliderComplex.setSnapToTicks(true);

        buttonNext.addActionListener(this);
        buttonPrec.addActionListener(this);
        buttonOK.addActionListener(this);
        Notifier.getInstance().addImageListener(this);
        Notifier.getInstance().addUserEventListener(this);
        Notifier.getInstance().notifyStartRequested();
        System.out.println(this);

        Image image = imageGetter.getCurrentImage();
        Objet[] o = image.getObjets();
        if (o != null) {
            spinnerPanneaux.setValue(o[0].getX());
            String temp = "";
            for (Objet ob : o) {
                temp += ob.getContenu() + "\n";
            }
            textPanePanneaux.setText(temp);
        }
    }

    public static void getInstance() {
        frame.setContentPane(new EvaluationUI().rootPanel);
        frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (buttonNext.equals(e.getSource())) {
            Image image = imageGetter.getCurrentImage();
            int nbrPanneaux = ((Integer) spinnerPanneaux.getValue());
            String[] test = textPanePanneaux.getText().split("\r\n");
            //Image image = imageGetter.getCurrentImage();
            if (imageList.contains(image)) {
                imageList.remove(image);
            }
            Image i = new Image();
            i.setNom(image.getNom());
            for (String t : test)
                i.addObjet(new Objet(nbrPanneaux, t, "" + sliderComplex.getValue()));
            imageList.add(i);
            Notifier.getInstance().notifyNextPictureRequested();
            image = imageGetter.getCurrentImage();
            Objet[] o = image.getObjets();
            if (o != null) {
                spinnerPanneaux.setValue(o[0].getX());
                String temp = "";
                for (Objet ob : o) {
                    temp += ob.getContenu() + "\n";
                }
                textPanePanneaux.setText(temp);
            }
        } else if (buttonPrec.equals(e.getSource())) {
        } else if (buttonOK.equals(e.getSource())) {
            try {
                mapper.defaultPrettyPrintingWriter().writeValue(file, imageList);
            } catch (IOException err) {
                System.out.println(err);
            }
            Notifier.getInstance().notifyStopRequested();
            frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
        }
    }

    @Override
    public void OnFeatureDetected() {
    }

    public void OnNextPictureRequested() {

    }

    @Override
    public void OnStopRequested() {
    }

    public void OnStartRequested() {
    }

    @Override
    public void OnPauseRequested() {
    }

    @Override
    public void onImageChanged(BufferedImage image, float zoom) {
    }
}