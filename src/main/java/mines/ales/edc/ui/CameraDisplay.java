package mines.ales.edc.ui;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamEvent;
import com.github.sarxos.webcam.WebcamListener;

import java.awt.event.WindowEvent;
import java.util.Timer;
import java.util.TimerTask;

public class CameraDisplay extends ImageDisplay implements WebcamListener {
    float zoom = 0f;
    private Webcam webcam;

    public CameraDisplay(Webcam webcam) {
        webcam.addWebcamListener(this);
        webcam.open();
        this.webcam = webcam;
        Timer timer = new Timer();
        timer.schedule(new CamTask(), 0, 10);
    }

    @Override
    public int getPositionEcran() {
        return 2;
    }

    public void setZoom(float zoom) {
        this.zoom = zoom;
    }

    @Override
    public void webcamOpen(WebcamEvent we) {
    }

    @Override
    public void webcamClosed(WebcamEvent we) {
    }

    @Override
    public void webcamDisposed(WebcamEvent we) {
    }

    @Override
    public void webcamImageObtained(WebcamEvent we) {
        onImageChanged(we.getImage(), zoom);
    }

    public void stop() {
        webcam.close();
        frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
        frame.dispose();
    }

    private class CamTask extends TimerTask {
        @Override
        public void run() {
            webcam.getImage();
        }
    }
}
