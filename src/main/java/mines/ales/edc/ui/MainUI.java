package mines.ales.edc.ui;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamResolution;
import ij.ImagePlus;
import mines.ales.edc.callback.Notifier;
import mines.ales.edc.callback.UserEventListener;
import mines.ales.edc.controller.Factory;
import mines.ales.edc.controller.model.ImageGetter;
import mines.ales.edc.controller.model.ResultWriter;
import mines.ales.edc.model.Image;
import mines.ales.edc.model.Objet;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Formatter;
import java.util.List;

public class MainUI implements ChangeListener, ActionListener, UserEventListener, MouseListener {
    long timeSinceLastFeatureDetected;
    CameraDisplay cameraDisplay;
    private JTextField nomClient;
    private JTextField date;
    private JButton startButton;
    private JButton stopButton;
    private JPanel rootPanel;
    private JSlider sliderRouge;
    private JSlider sliderVert;
    private JSlider sliderBleu;
    private JSlider sliderLuminosité;
    private JButton buttonCorrect;
    private JList listObjet;
    private JSlider sliderZoom;
    private JSlider sliderZoomLunette;
    private JButton lunettesButton;
    private ImageGetter imageGetter = ImageGetter.getInstance();
    private boolean isExerciceStarted = false;
    private boolean isLunetteStarted = false;
    /**
     * C'est le timer entre l'affichage de l'image et la réponse du patient.
     */
    private long timer;
    SimpleDateFormat formater = new SimpleDateFormat("yyyyMMdd");

    public MainUI() {
        date.setText(formater.format(new Date()));
        sliderRouge.setValue(255);
        sliderVert.setValue(255);
        sliderBleu.setValue(255);
        sliderLuminosité.setValue(255);
        sliderRouge.addChangeListener(this);
        sliderVert.addChangeListener(this);
        sliderBleu.addChangeListener(this);
        sliderLuminosité.addChangeListener(this);
        sliderZoom.addChangeListener(this);
        sliderZoomLunette.addChangeListener(this);
        buttonCorrect.addActionListener(this);
        startButton.addActionListener(this);
        stopButton.addActionListener(this);
        lunettesButton.addActionListener(this);
        listObjet.addMouseListener(this);
        ImageDisplay imageDisplay = new ImageDisplay();
        Notifier.getInstance().addImageListener(imageDisplay);
        imageDisplay.getInstance();
        switchButtonsState();
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {
            public boolean dispatchKeyEvent(KeyEvent e) {
                long actualTime = System.currentTimeMillis();
                if (e.getKeyCode() == KeyEvent.VK_SPACE && !isExerciceStarted) {
                    if ((actualTime - timeSinceLastFeatureDetected) >= 500)
                        Notifier.getInstance().notifyFeatureDetected();
                    timeSinceLastFeatureDetected = actualTime;
                    return true;
                }
                return false;
            }
        });
        Notifier.getInstance().addUserEventListener(this);
    }

    public static void getInstance() {
        JFrame frame = new JFrame("MainUI");

        JMenuBar jMenuBar = new JMenuBar();
        JMenu evalMenu = new JMenu("Evaluation");
        JMenuItem evalMenuItem = new JMenuItem("Evalue les images");
        evalMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                //addImageListener(evaluationUI);
                EvaluationUI.getInstance();
            }
        });
        evalMenu.add(evalMenuItem);
        jMenuBar.add(evalMenu);
        frame.setJMenuBar(jMenuBar);

        frame.setContentPane(new MainUI().rootPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public void switchButtonsState() {
        sliderRouge.setEnabled(isExerciceStarted);
        sliderVert.setEnabled(isExerciceStarted);
        sliderBleu.setEnabled(isExerciceStarted);
        sliderLuminosité.setEnabled(isExerciceStarted);
        buttonCorrect.setEnabled(isExerciceStarted);
        stopButton.setEnabled(isExerciceStarted);
        listObjet.setEnabled(isExerciceStarted);
        startButton.setEnabled(!isExerciceStarted);
        nomClient.setEnabled(!isExerciceStarted);
        date.setEnabled(!isExerciceStarted);
        isExerciceStarted = !isExerciceStarted;
    }


    /**
     * Changement d'état des sliders
     */
    @Override
    public void stateChanged(ChangeEvent e) {
        JSlider source = (JSlider) e.getSource();
        if (source.equals(sliderZoomLunette)) {
            cameraDisplay.setZoom(source.getValue());
        } else if (!source.getValueIsAdjusting()) {
            refreshImage();
        }
    }

    void modifyImage(final ImagePlus image) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Factory.getImageModifier().changeRGB(image,
                        255 - sliderRouge.getValue(), 255 - sliderVert.getValue(), 255 - sliderBleu.getValue());
                Factory.getImageModifier().changeLuminosity(image, 255 - sliderLuminosité.getValue());
                Notifier.getInstance().notifyImageChanged(image.getBufferedImage(), sliderZoom.getValue());
            }
        }).start();
    }


    /**
     * Changement d'état des boutons
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (buttonCorrect.equals(e.getSource())) {
            Notifier.getInstance().notifyNextPictureRequested();
        } else if (startButton.equals(e.getSource())) {
            Notifier.getInstance().notifyStartRequested();
        } else if (stopButton.equals(e.getSource())) {
            Notifier.getInstance().notifyStopRequested();
        } else if (lunettesButton.equals(e.getSource())) {
            manageLunette();
        }
    }

    private void manageLunette() {
        if (!isLunetteStarted) {
            Webcam webcam = Webcam.getWebcams().get(1);
            webcam.setViewSize(WebcamResolution.VGA.getSize());
            cameraDisplay = new CameraDisplay(webcam);
            cameraDisplay.getInstance();
        } else {
            cameraDisplay.stop();
        }
        isLunetteStarted = !isLunetteStarted;
    }

    private void refreshImage() {
        Image image = imageGetter.getCurrentImage();
        if(image.getObjets() != null) {
            listObjet.setListData(image.getObjets());
        }
        modifyImage(image.getImagePlus());
    }

    @Override
    public void OnFeatureDetected() {
        System.out.println("MainUI.OnFeatureDetected");
    }

    @Override
    public void OnNextPictureRequested() {
        List<Objet> objets = new ArrayList<Objet>();
        for (int i = 0; i < listObjet.getModel().getSize(); i++) {
            objets.add((Objet) listObjet.getModel().getElementAt(i));
        }
        Image image = imageGetter.getCurrentImage();
        image.setObjets(objets);
        ResultWriter.getInstance().write(image, System.currentTimeMillis() - timer);
        timer = System.currentTimeMillis();
        if (imageGetter.hasNextImage()) {
            imageGetter.goToNextImage();
            refreshImage();
        } else {
            stopButton.setEnabled(false);
        }
    }

    @Override
    public void OnStopRequested() {
        switchButtonsState();
    }

    @Override
    public void OnStartRequested() {
        switchButtonsState();
        ResultWriter.getInstance().init(nomClient.getText(), date.getText());
        Notifier.getInstance().notifyNextPictureRequested();
        timer = System.currentTimeMillis();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 1) {
            JList list = (JList) e.getSource();
            int position = list.locationToIndex(e.getPoint());
            ((Objet) listObjet.getModel().getElementAt(position)).swapDetecter();
            listObjet.repaint();
        }
    }

    @Override
    public void OnPauseRequested() {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
