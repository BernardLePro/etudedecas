package mines.ales.edc;

import mines.ales.edc.controller.model.ImageGetter;
import mines.ales.edc.ui.MainUI;

public class Main {

    public static void main(String[] args) {
        ImageGetter.getInstance().getImagesFromFolder();
        MainUI.getInstance();
    }
}
