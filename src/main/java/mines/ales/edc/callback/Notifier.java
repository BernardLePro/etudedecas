package mines.ales.edc.callback;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class Notifier {
    private List<ImageListener> imageListeners;
    private List<UserEventListener> userEventListeners;

	public static Notifier getInstance() {
        return SingletonHolder.instance;
    }
	
    public void addImageListener(ImageListener imageListener) {
        if (imageListeners == null)
            imageListeners = new ArrayList<ImageListener>();
        imageListeners.add(imageListener);

    }

    public void removeImageListener(ImageListener imageListener) {
        if (imageListeners != null)
            imageListeners.remove(imageListener);
    }

    public void notifyImageChanged(BufferedImage image, float zoom) {
        if (imageListeners != null)
            for (ImageListener imageListener : imageListeners) {
                imageListener.onImageChanged(image, zoom);
            }
    }

    public void addUserEventListener(UserEventListener userEventListener) {
        if (userEventListeners == null)
            userEventListeners = new ArrayList<UserEventListener>();
        userEventListeners.add(userEventListener);
    }

    public void removeUserEventListener(UserEventListener userEventListener) {
        if (userEventListeners != null)
            userEventListeners.remove(userEventListener);
    }

    public void notifyFeatureDetected() {
        if (userEventListeners != null)
            for (UserEventListener userEventListener : userEventListeners) {
                userEventListener.OnFeatureDetected();
            }
    }

    public void notifyNextPictureRequested() {
        if (userEventListeners != null)
            for (UserEventListener userEventListener : userEventListeners) {
                userEventListener.OnNextPictureRequested();
            }
    }

//	public void notifyPreviousPictureRequested() {
//        if (userEventListeners != null)
//            for (UserEventListener userEventListener : userEventListeners) {
//                userEventListener.OnPreviousPictureRequested();
//            }
//    }
    public void notifyStopRequested() {
        if (userEventListeners != null)
            for (UserEventListener userEventListener : userEventListeners) {
                userEventListener.OnStopRequested();
            }
    }

    public void notifyStartRequested() {
        if (userEventListeners != null)
            for (UserEventListener userEventListener : userEventListeners) {
                userEventListener.OnStartRequested();
            }
    }

    public void notifyPauseRequested() {
        if (userEventListeners != null)
            for (UserEventListener userEventListener : userEventListeners) {
                userEventListener.OnPauseRequested();
            }
    }
	private static class SingletonHolder {
        private final static Notifier instance = new Notifier();
    }
}
