package mines.ales.edc.callback;

import java.awt.image.BufferedImage;

public interface ImageListener {
    /**
     * @param zoom varie entre 0 et 100
     */
    void onImageChanged(BufferedImage image, float zoom);
}
