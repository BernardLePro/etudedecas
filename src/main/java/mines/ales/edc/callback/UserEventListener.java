package mines.ales.edc.callback;

public interface UserEventListener {
    public void OnFeatureDetected();

    public void OnNextPictureRequested();
	
    public void OnStopRequested();

    public void OnStartRequested();

    public void OnPauseRequested();
}
